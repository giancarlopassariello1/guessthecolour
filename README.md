# GuessTheColour

Project Structure

The project consists of HTML, CSS, and JavaScript files. Below is a brief description of the key files in the project:

- index.html: The main HTML file that contains the structure of the web application. It includes the game setup form, the game board, and the scoreboard.
- style.css and mobile.css: These CSS files define the styling of the web application for both desktop and mobile devices.
- setup.js: This JavaScript file handles the setup of the game, including form validation, starting the game, and enabling the cheat code.
- score.js: Manages the scoreboard, sorting scores, and displaying user scores.
- utilities.js: Provides utility functions used throughout the application, such as counting selected tiles and calculating scores.
- board.js: Contains functions related to creating the game board and handling tile selection.

---------------------------------------------------------------------------------------------------------------------------------------------------
How to Run the Project

To run the project, follow these steps:

1. Download all the project files, including HTML, CSS, and JavaScript files.
2. Open the index.html file in a web browser (Google Chrome, Mozilla Firefox, etc.).
3. The web application will load, and you can begin by entering your player name, choosing the game board size, selecting a color, and setting the difficulty level.
4. Click the "Start Game!" button to start the game.
5. Select tiles on the game board to match the chosen color. The goal is to match the number of tiles displayed in the "Your target is" message.
6. After selecting your tiles, click the "Submit your guess!" button to calculate your score and see if you won or lost.
7. You can also enable a cheat code by simultaneously pressing the "Shift" and "C" keys, which will reveal color information in the table cells.
8. The game includes a scoreboard, and you can sort it from the highest score to the lowest or vice versa by clicking the "Score" button.

---------------------------------------------------------------------------------------------------------------------------------------------------

