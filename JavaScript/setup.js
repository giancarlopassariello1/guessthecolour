"use strict";

function setup(e){
  let scoreData = [];
  if(JSON.parse(localStorage.getItem("scoreBoard")) !== null){
      scoreData = JSON.parse(localStorage.getItem("scoreBoard"));
  }

  if(scoreData.length > 0){
      for(let i = 0; i< scoreData.length; i++){
        addScore(scoreData[i].name, scoreData[i].score);
      }
  }
  const clearButton = document.getElementsByTagName("button")[2];
  const startButton = document.getElementsByTagName("button")[0];
  const tableDiv = document.getElementById("TableDiv");
  const searchColour = document.getElementById("searchColour");
  const submitGuessButton = document.getElementById("submitGuessButton");
  const selectColour = document.getElementById("colourPicked");
  const scoreButton = document.getElementById("scoreButton");
  submitGuessButton.style.visibility = "hidden"
  submitGuessButton.disabled = true;

  let shiftPressed = false;
  let cPressed = false;
  let isSubmitGuessButtonDisabled = true;

  /**
   * Event listener for the "keydown" event to detect "shift key" and "c key" presses.
   *
   * @param {KeyboardEvent} e - The keydown event object.
  */
  document.addEventListener("keydown", (e) => {
      if (e.key === "Shift") {
        shiftPressed = true;
      }
      if (e.key === "c" || e.key === "C") {
        cPressed = true;
      }
      cheatCode();
  });

  /**
   * Event listener for the "keyup" event to detect "shift key" and "c key" releases.
   *
   * @param {KeyboardEvent} e - The keyup event object.
  */
  document.addEventListener("keyup", (e) => {
        if (e.key === "Shift") {
          shiftPressed = false;
        }
        if (e.key === "c" || e.key === "C") {
          cPressed = false;
        }
  });
    
  /**
 * Activates a cheat code to reveal colour information in table cells when Shift and C keys are pressed simultaneously. 
 * It also checks if the cell is too light or too dark so that the text colour will be visible.
 *
 */  
  function cheatCode() {
      const cells = Array.from(document.getElementsByTagName("td"));
      if (shiftPressed && cPressed) {
        for(let cell of cells){
            if(cell.textContent === ""){
              if (isLight(cell.style.backgroundColor)) {
                cell.textContent = `${cell.style.backgroundColor}\n${colourFinder(cell.style.backgroundColor)}`;
                cell.style.textAlign = "center";
              } else {
                cell.textContent = `${cell.style.backgroundColor}\n${colourFinder(cell.style.backgroundColor)}`;
                cell.style.textAlign = "center";
                cell.style.color = "white";
              }
            }
            else if(cell.textContent !== ""){
                cell.textContent = "";
            }
        }
      }
  }
  
  
  /**
 * Starts the game by processing user input, creating a table, and providing game information.
 *
 * This function is called when the user clicks on the start game button. It disables the submit guess button,
 * prevents form submission, validates the form input, and creates a table based on user preferences.
 * If the form is filled out correctly, it updates the game setup information, blocks further setup changes,
 * and displays information about the search for tiles of the selected colour.
 *
 * @param {Event} e - The event object triggered by the form submission.
 */
  function startGame(e){
    submitGuessButton.style.visibility = "visible"
    isSubmitGuessButtonDisabled = true;
    const h2 = document.getElementsByTagName("h2")[0];
    e.preventDefault();
    if(!validateSetup()){
      h2.textContent = "Please fill out the form correctly!";
      return;
    }
    h2.textContent = "Game setup";
    const colourPicked = document.getElementById("colourPicked").value;
    const difficulty = document.getElementById("difficulty").value;
    const sizeInput = document.getElementById("sizeInput").value;
    createTable(sizeInput, difficulty);
    blockSetup();
    searchColour.textContent = `Searching for ${colourPicked} tiles! Your target is ${countColour(colourPicked)} tiles!`;
  }
  startButton.addEventListener("click", startGame);
   
  /**
 * Event listener for the "click" event on the game table, allowing the player to select a tile.
 *
 * @param {Event} e - The click event object.
 */
  tableDiv.addEventListener("click", (e) => {
    const colourPicked = document.getElementById("colourPicked").value;
    const cellClicked = e.target;
    if (cellClicked.tagName != "TD") {
      return;
    }
    selectTile(colourPicked, cellClicked, isSubmitGuessButtonDisabled, submitGuessButton);
  });

  /**
 * Handles the submission of a player's guess, calculates their score, and updates the score list.
 *
 * This function is called when a player submits their guess. It enables the submit guess button,
 * validates the game board, updates the game setup, retrieves player, validates
 * the selected tiles, calculates the score, and adds the player's score to the score list. It also
 * sorts the score list based on player scores.
 *
 * @param {Event} e - The event object triggered by the form submission.
 */
  function submitGuess (e) {
    isSubmitGuessButtonDisabled = false;
    submitGuessButton.disabled = true;
    if (!validateBoard()) {
      return;
    }
    deblockSetup();
    const name = document.getElementById("name").value;
    const colourPicked = document.getElementById("colourPicked").value;
    const sizeInput = document.getElementById("sizeInput").value;
    const difficulty = document.getElementById("difficulty").value;
    const selectedTiles = Array.from(document.querySelectorAll(".selected"));
    const numCorrect = validateGuess(selectedTiles, colourPicked); 
    const numSelected = countSelected();
    const score = getScore(numCorrect, numSelected, sizeInput, difficulty);
        
    addScore(name, score);

    const playerNameList = Array.from(document.getElementById("playerNameList").children);
    const scoreList = Array.from(document.getElementById("scoreList").children);
    showScore(numCorrect, numSelected, score);
        
    if (changeScoreSorting()) {
      sortHighestToLowest(playerNameList, scoreList);    
    } else {
      sortLowestToHighest(playerNameList, scoreList);
    }
              
  }

  submitGuessButton.addEventListener("click", submitGuess);

  /**
 * Event listener for the "click" event on the clear button, clearing the score list and local storage.
 *
 * @param {Event} e - The click event object triggered by clicking the clear button.
 */
  clearButton.addEventListener("click", (e) =>{
    clearScore();
  });

  /**
 * Event listener for the "change" event on the select element, updating the start button's appearance.
 *
 * @param {Event} e - The change event object triggered when a new colour is selected.
 */
  selectColour.addEventListener("change", (e) => {
    const option1 = selectColour.querySelector("option");
    option1.style.display = "none";
    startButton.style.backgroundColor = selectColour.value;
    startButton.style.color = "white";
  });

  /**
 * Event listener for the "click" event on the score button, toggling and sorting the score list display.
 *
 * @param {Event} e - The click event object triggered by clicking the score button.
 */
  scoreButton.addEventListener("click", function (e) {
    const playerNameList = Array.from(document.getElementById("playerNameList").children);
    const scoreList = Array.from(document.getElementById("scoreList").children);
    if (changeScoreSorting()) {
      scoreButton.classList.remove("highest");
      scoreButton.classList.add("lowest");
      sortLowestToHighest(playerNameList, scoreList);
    } else {
      scoreButton.classList.remove("lowest");
      scoreButton.classList.add("highest");
      sortHighestToLowest(playerNameList, scoreList);
    }
      
  });
}

document.addEventListener("DOMContentLoaded", setup);

/**
 * Enables user interaction with the game setup elements.
 *
 */
function deblockSetup(){
  const colourPicked = document.getElementById("colourPicked");
  const sizeInput = document.getElementById("sizeInput");
  const difficulty = document.getElementById("difficulty");
  const name = document.getElementById("name");
  const startButton = document.getElementsByTagName("button")[0];

  startButton.disabled = false;
  colourPicked.disabled = false;
  sizeInput.disabled = false;
  difficulty.disabled = false;
  name.disabled = false;
}

/**
 * Disables user interaction with the game setup elements.
 *
 */
function blockSetup(){
const colourPicked = document.getElementById("colourPicked");
const sizeInput = document.getElementById("sizeInput");
const difficulty = document.getElementById("difficulty");
const name = document.getElementById("name");
const startButton = document.getElementsByTagName("button")[0];

startButton.disabled = true;
colourPicked.disabled = true;
sizeInput.disabled = true;
difficulty.disabled = true;
name.disabled = true;
}

/**
 * Validates the game setup input provided by the user.
 *
 * @returns {boolean} - True if the game setup input is valid; false otherwise.
 */
function validateSetup() {
  const sizeInput = document.getElementById("sizeInput");
  const name = document.getElementById("name");
  const colourPicked = document.getElementById("colourPicked").value;
  if (sizeInput.checkValidity() && name.checkValidity() && colourPicked !== "") {
      return true;
  } else {
      return false;
  }
}