"use strict";

/**
 * Finds the dominant colour from an RGB string.
 *
 * This function takes an RGB string and determines the dominant colour based on the
 * red, green, and blue values. 
 *
 * @param {string} rgbString - An RGB string in the format "rgb(r, g, b)".
 * @returns {string} - The dominant colour: "red", "green", or "blue".
*/

function colourFinder(rgbString){
    rgbString = rgbString.slice(4, -1);
    let rgbValues = rgbString.split(',');
    let r = parseInt(rgbValues[0]);
    let g = parseInt(rgbValues[1]);
    let b = parseInt(rgbValues[2]);
  
    if(r > g && r > b){
        return "red";
    }
    else if(g > r && g > b){
        return "green";
    }
    else{
        return "blue";
    }
    
}

/**
 * Counts the number of cells with a specific colour.
 *
 * This function takes a specific colour ("red", "green", or "blue") as input and counts
 * the number of td elements that have the specified background colour
 * matching the given colour.
 *
 * @param {string} colour - The colour to count ("red", "green", or "blue").
 * @returns {number} - The number of cells with the specified colour.
 */
function countColour(colour){
    let count = 0;
    const cells = Array.from(document.getElementsByTagName("td"));
    if(colour === "red"){
        for(let cell of cells){
            if(colourFinder(cell.style.backgroundColor) === "red"){
                count++;
            }
        }
    } else if(colour === "green"){
        for(let cell of cells){
            if(colourFinder(cell.style.backgroundColor) === "green"){
                count++;
            }
        }
    } else {
        for(let cell of cells){
            if(colourFinder(cell.style.backgroundColor) === "blue"){
                count++;
            }
        }
    }
  
    return count;
}

/**
 * Generates a random number within a specified range.
 *
 * @param {number} min - The minimum value of the desired range (inclusive, >= 0).
 * @param {number} max - The maximum value of the desired range (exclusive, <= 255).
 * @returns {number} - A random integer within the specified range.
 */
function getRandomNum(min, max){
    if (max > 255) {
        max = 255;
    } 
    if (min < 0) {
        min = 0;
    }
    return Math.floor((Math.random() * (max - min) + min));
}

/**
 * Generates a random colour in RGB format based on the specified difficulty level.
 *
 * @param {number} difficulty - The difficulty level (0, 1, 2, or 3) that determines the color range.
 * @returns {string} - A random RGB colour in the format "rgb(r, g, b)".
 */
function randomColour(difficulty){
    let colour = "";
    let red = 0;
    let green = 0;
    let blue = 0;
  
    let range = 0;
    
    if(+difficulty === 0){
        red = getRandomNum(0, 255);
        green = getRandomNum(0, 255);
        blue = getRandomNum(0, 255);
  
        colour = `rgb(${red},${green},${blue})`;
    }
    else if(+difficulty === 1){
        range = getRandomNum(0, 255);
        red = getRandomNum(range-40, range+40);
        green = getRandomNum(range-40, range+40);
        blue = getRandomNum(range-40, range+40);
  
        colour = `rgb(${red},${green},${blue})`;
    }
    else if(+difficulty === 2){
        range = getRandomNum(0, 255);
        red = getRandomNum(range-20, range+20);
        green = getRandomNum(range-20, range+20);
        blue = getRandomNum(range-20, range+20);
  
        colour = `rgb(${red},${green},${blue})`;
    }
    else if(+difficulty === 3){
        range = getRandomNum(0, 255);
        red = getRandomNum(range-5, range+5);
        green = getRandomNum(range-5, range+5);
        blue = getRandomNum(range-5, range+5);
  
        colour = `rgb(${red},${green},${blue})`;
    }
  
    return colour;
}

/**
 * Determines if the background colour, represented in an RGB string, is considered light.
 *
 * This function takes an RGB string, extracts the RGB values, and calculates the luminance
 * of the colour. It then compares the luminance to a specified threshold (0.5) to determine if
 * the colour is considered light. If the colour is light, it returns true; otherwise, it returns
 * false.
 *
 * @param {string} rgbString - An RGB string in the format "rgb(r, g, b)".
 * @returns {boolean} - True if the colour is considered light; false otherwise.
 */
function isLight(rgbString) {
    rgbString = rgbString.slice(4, -1);
    let rgbValues = rgbString.split(',');
    let r = parseInt(rgbValues[0]) / 255;
    let g = parseInt(rgbValues[1]) / 255;
    let b = parseInt(rgbValues[2]) / 255;
    const luminance = (0.2126 * r) + (0.7152 * g) + (0.0722 * b);
    const threshold = 0.5;

    if (luminance >= threshold) {
        return true;
    } else {
        return false;
    }
}
