"use strict";

/**
 * Validates the guess by comparing the selected tiles' colours with the picked colour.
 *
 * @param {Array} selectedTiles - A list of selected tile elements.
 * @param {string} colourPicked - The colour chosen by the user for validation.
 * @returns {number} The number of tiles that match the picked colour.
 */
function validateGuess(selectedTiles, colourPicked) {
    let correctNums = 0;
    selectedTiles.forEach((tile) => {
        if (colourFinder(tile.style.backgroundColor) === colourPicked) {
            correctNums++;
        }
    });

    return correctNums;

}

/**
 * Calculates the score based on the correct guesses and selected tiles.
 *
 * @param {number} numCorrect - The number of correct tiles selected.
 * @param {number} numSelected - The total number of tiles selected.
 * @param {number} sizeInput - The size of the game board.
 * @param {number} difficulty - The difficulty level of the game.
 * @returns {number} The calculated score.
 */
function getScore(numCorrect, numSelected, sizeInput, difficulty) {
  const percent = (2 * numCorrect - numSelected) / (sizeInput * sizeInput);
  return Math.floor(percent * 100 * sizeInput * (difficulty + 1));
}

/**
 * Clears the score display on the scoreboard and the local storage.
 */
function clearScore(){
    const playerNameOl = document.getElementById("playerNameList");
    const scoreOl = document.getElementById("scoreList");

    playerNameOl.textContent = "";
    scoreOl.textContent = "";
    localStorage.clear();
}

/**
 * Adds a player's score to the scoreboard.
 *
 * @param {string} name - The name of the player.
 * @param {number} score - The score of the player.
 */

function addScore(name, score){
    const playerNameOl = document.getElementById("playerNameList");
    const scoreOl = document.getElementById("scoreList");
    const liName = document.createElement("li");
    const liScore = document.createElement("li");

    liName.textContent = name;
    liScore.textContent = score;

    playerNameOl.appendChild(liName);
    scoreOl.appendChild(liScore);
}

/**
 * Sorts the score data from highest to lowest and updates the scoreboard.
 *
 * @param {Array} playerNameList - A list of player name elements.
 * @param {Array} scoreList - A list of score elements.
 */
function sortHighestToLowest(playerNameList, scoreList){
    let scoreData = [];
    let count = 0;
    for(let i = 0; i < playerNameList.length; i++){
        if(count < 10){
            count++;
        }
        const participant = {
            name: playerNameList[i].textContent,
            score: scoreList[i].textContent
        };
        scoreData.push(participant);
    }

    const playerNameOl = document.getElementById("playerNameList");
    const scoreOl = document.getElementById("scoreList");
    scoreOl.textContent = "";
    playerNameOl.textContent = "";
    scoreData.sort(function(a, b){return b.score-a.score});

    for(let i = 0; i < count; i++){
        addScore(scoreData[i].name, scoreData[i].score);
    }
    if (scoreData.length > 10) {
        for (let i = 0; i < scoreData.length - count; i++) {
            scoreData.pop();
        }
    }
    const jsonString = JSON.stringify(scoreData);
    localStorage.setItem("scoreBoard", jsonString);
}

/**
 * Sorts the score data from lowest to highest and updates the scoreboard.
 *
 * @param {Array} playerNameList - A list of player name elements.
 * @param {Array} scoreList - A list of score elements.
 */
function sortLowestToHighest(playerNameList, scoreList) {
    let scoreData = [];
    let count = 0;
    for(let i = 0; i < playerNameList.length; i++){
        if(count < 10){
            count++;
        }
        const participant = {
            name: playerNameList[i].textContent,
            score: scoreList[i].textContent
        };
        scoreData.push(participant);
    }

    const playerNameOl = document.getElementById("playerNameList");
    const scoreOl = document.getElementById("scoreList");
    scoreOl.textContent = "";
    playerNameOl.textContent = "";
    scoreData.sort(function(a, b){return b.score-a.score});

    for(let i = count; i > 0; i--){
        addScore(scoreData[i-1].name, scoreData[i-1].score);
    }
    const jsonString = JSON.stringify(scoreData);
    localStorage.setItem("scoreBoard", jsonString);
}

/**
 * Checks the current score sorting preference.
 *
 * @returns {boolean} True if the highest score sorting is active, otherwise false.
 */
function changeScoreSorting() {
    const score = document.getElementById("scoreButton");
    if (score.classList.contains("highest")) {
        return true;
    } else {
        return false;
    }
}

/**
 * Displays the score to the user along with a message indicating if they won or lost.
 *
 * @param {number} numCorrect - The number of correct guesses made by the user.
 * @param {number} numSelected - The total number of guesses made by the user.
 * @param {number} score - The calculated score for the user.
 */
function showScore(numCorrect, numSelected, score) {
    const searchColour = document.getElementById("searchColour");
    const percentCorrect = (numCorrect / numSelected) * 100;
    if (percentCorrect >= 60) {
        searchColour.textContent = `You won! You got ${Math.floor(percentCorrect)}% correct! You got a score of ${score}`;
    } else {
        searchColour.textContent = `You lost! You only got ${Math.floor(percentCorrect)}% correct! You got a score of ${score}`;
    }
}
