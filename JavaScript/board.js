"use strict";

/**
 * Creates a table with the specified size and difficulty level.
 *
 * @param {number} size - The size of the table (number of rows and columns).
 * @param {string} difficulty - The difficulty level to determine cell colours.
 */
function createTable(size, difficulty){
    const tbody = document.getElementsByTagName("tbody")[0];
    tbody.textContent = "";
    for(let i = 0; i < size; i++){
        //for loop for the rows
        const createRow = document.createElement("tr");
        tbody.appendChild(createRow);
        for(let j = 0; j < size; j++){
            //for loop to put the columns in the rows
            const row = document.getElementsByTagName("tr")[i];
            const createColumn = document.createElement("td");
            createColumn.classList.add("unselected");
            createColumn.style.backgroundColor = randomColour(difficulty);
            row.appendChild(createColumn);
        }
    }
}

/**
 * Handles tile selection based on the colour picked.
 *
 * @param {string} colourPicked - The colour selected by the player.
 * @param {HTMLTableCellElement} cellClicked - The clicked cell element.
 * @param {boolean} isSubmitGuessButtonDisabled - Indicates if the submit guess button is disabled.
 * @param {HTMLButtonElement} submitGuessButton - The submit guess button element.
 */
function selectTile(colourPicked, cellClicked, isSubmitGuessButtonDisabled, submitGuessButton) {
    if (isSubmitGuessButtonDisabled) {
        if (countColour(colourPicked) > countSelected()) {
            if (cellClicked.classList.contains("unselected")) {
                cellClicked.classList.remove("unselected");
                cellClicked.classList.add("selected");
            } else {
                cellClicked.classList.remove("selected");
                cellClicked.classList.add("unselected");
            }
        } else if (countColour(colourPicked) === countSelected()) {
                cellClicked.classList.remove("selected");
                cellClicked.classList.add("unselected");
        }
        
        if (countColour(colourPicked) > countSelected()) {
          submitGuessButton.disabled = true;
        } else if (countColour(colourPicked) === countSelected()) {
          submitGuessButton.disabled = false;
        }
    }

    searchColour.textContent = `Searching for ${colourPicked} tiles! Your target is ${countColour(colourPicked)} tiles! ${countSelected()} selected!`;
   
}

/**
 * Counts the number of selected cells in the table.
 *
 * @returns {number} The count of selected cells.
 */
function countSelected() {
    let count = 0;
    const cells = Array.from(document.getElementsByTagName("td"));
    for(let cell of cells){
        if(cell.classList.contains("selected")){
            count++;
        }
    }
    return count;
}

/**
 * Validates the board by comparing the number of selected cells with the target count.
 *
 * @returns {boolean} Returns true if the board is valid, otherwise false.
 */
function validateBoard() {
    const colourPicked = document.getElementById("colourPicked").value;
    if (countColour(colourPicked) === countSelected()) {
        return true;
    } else {
        return false;
    }
}